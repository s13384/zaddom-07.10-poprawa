
public class TwoForOneDiscount implements Discount {

	@Override
	public void discount(Cart cart) {
		
		for (CartPosition cartPosition : cart.getCart())
		{
			// znizka obowiazuje co dwa produkty np. za 2 placi sie jak za 1  a za 4 jak za 2
			if (cartPosition.getQuantity()>=2)
			{
				int numberOfPairs=cartPosition.getQuantity()/2;
					
				cartPosition.setTotalPrice(cartPosition.getTotalPrice()-cartPosition.getProduct().getPrice()*numberOfPairs);
			}
		}
	}

	@Override
	public boolean checkDiscount(Cart cart) {
		
		for (CartPosition cartPosition : cart.getCart())
		{
			if (cartPosition.getQuantity()>=2)
			{
				return true;
			}
		}
		
		return false;
	}

}
