
public class CartPosition {

	private int quantity;
	private float totalPrice;
	private Product product;
	
	CartPosition(Product product, int q_ty)
	{
		this.product=product;
		quantity=q_ty;
		totalPrice=quantity*product.getPrice();
	}
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
}
