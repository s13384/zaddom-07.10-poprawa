import java.util.ArrayList;


public class Cart {

	private ArrayList<CartPosition> positions;
	private Client client;
	private float totalPrice;
	static ArrayList<Discount> discounts;
	
	public ArrayList<CartPosition> getCart() {
		return positions;
	}
	public void addToCart(Product product, int quantity) {
		positions.add(new CartPosition(product, quantity));
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public static ArrayList<Discount> getDiscounts() {
		return discounts;
	}
	public static void setDiscounts(ArrayList<Discount> discounts) {
		Cart.discounts = discounts;
	}
	public void calculateDiscount()	{
		
		for (Discount discount : discounts)
		{
			if (discount.checkDiscount(this)) discount.discount(this); 
		}
	}
}
