import java.util.Calendar;
import java.util.Date;


public class ThuesdaysStudentDiscount implements Discount {

	@Override
	public void discount(Cart cart) {
		
			cart.setTotalPrice(cart.getTotalPrice()*3/4);

	}

	@Override
	public boolean checkDiscount(Cart cart) {
		
		Date date = new Date();
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		
		if (dayOfWeek==4)
		{
			if (cart.getClient().isStudent())
			{
				return true;
			}
		}
		return false;
	}

}
