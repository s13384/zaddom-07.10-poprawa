
public interface Discount {

	public void discount(Cart cart);
	public boolean checkDiscount(Cart cart);
}
